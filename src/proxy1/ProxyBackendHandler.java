package proxy1;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author Vinicius Carvalho
 * Taken from: https://github.com/netty/netty/blob/4.1/example/src/main/java/io/netty/example/proxy/HexDumpProxyBackendHandler.java
 */
public class ProxyBackendHandler extends ChannelInboundHandlerAdapter {

    //    private final Channel inboundChannel;
    private final ProxyBackendListener listener;

    public ProxyBackendHandler(ProxyBackendListener listener) {
        this.listener = listener;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        ctx.read();
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, Object msg) {
        listener.onRemoteRead(msg);
        ctx.channel().read();
//        inboundChannel.writeAndFlush(msg).addListener(new ChannelFutureListener() {
//            @Override
//            public void operationComplete(ChannelFuture future) {
//                if (future.isSuccess()) {
//                    try {
//                        ctx.channel().read();
//                    } catch (Exception e) {
//                        System.out.println(e.getMessage());
//                    }
//                } else {
//                    future.channel().close();
//                }
//            }
//        });
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        listener.onRemoteInactive(ctx);
        ProxyConnectionHandler.closeOnFlush(ctx.channel());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        listener.onRemoteError(cause);
        ProxyConnectionHandler.closeOnFlush(ctx.channel());
    }
}
