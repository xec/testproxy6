package proxy1;

import io.netty.channel.ChannelHandlerContext;

public interface ProxyBackendListener {

    public void onRemoteInactive(ChannelHandlerContext ctx);

    public void onRemoteRead(Object msg);

    public void onRemoteError(Throwable cause);
}
