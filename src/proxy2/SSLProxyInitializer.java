package proxy2;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;

public class SSLProxyInitializer extends ChannelInitializer {
    private Channel clientChannel;

    public SSLProxyInitializer(Channel clientChannel) {
        this.clientChannel = clientChannel;
    }

    @Override
    protected void initChannel(Channel ch) throws Exception {
        ch.pipeline().addLast(new SSLProxyClientHandle(clientChannel));
    }
}
