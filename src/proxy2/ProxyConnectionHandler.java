package proxy2;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.http.*;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

import java.net.URI;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Handles a server-side channel.
 */
public class ProxyConnectionHandler extends ChannelInboundHandlerAdapter {
    private ChannelFuture cf;
    public Channel local;
    private String host;
    private int port;

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (cf != null) cf.channel().close();
    }


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        local = ctx.channel();
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        if (cf != null) cf.channel().close();
    }


    private void connectToRemote(final ChannelHandlerContext ctx, FullHttpRequest request, ChannelInitializer initializer, GenericFutureListener<? extends Future<? super Void>> listener) {
        //连接至目标服务器
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(ctx.channel().eventLoop()) // 复用客户端连接线程池
                .channel(ctx.channel().getClass()) // 使用NioSocketChannel来作为连接用的channel类
                .handler(initializer);
        cf = bootstrap.connect(host, port);
        cf.addListener(listener);
    }

    private FullHttpResponse createContent(byte[] body) {
        FullHttpResponse resp = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, Unpooled.wrappedBuffer(body));
        resp.headers().set("Content-Type", "text/html");
        resp.headers().set("Content-Length", body.length);
        resp.headers().set("Connection", "close");
        return resp;

    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof FullHttpRequest) {
            FullHttpRequest request = (FullHttpRequest) msg;
            String url = request.uri();
            if (request.method() == HttpMethod.CONNECT) {
                url = "http://" + request.uri();
            }

            URI uri = URI.create(url);
            this.host = uri.getHost();
            this.port = uri.getPort() == -1 ? 80 : uri.getPort();

            if (request.method() != HttpMethod.CONNECT) {
                url = url.substring(url.indexOf("//") + 2);
                url = url.substring(url.indexOf("/"));
            }

            request.setUri(url);

            // remove proxy
            Set<String> proxyHeaders = new HashSet<>();
            for (Map.Entry<String, String> head : request.headers()) {
                if (head.getKey().toLowerCase().startsWith("proxy")) {
                    proxyHeaders.add(head.getKey());
                }
            }

            for (String head : proxyHeaders) {
                request.headers().remove(head);
            }

            if (host.contains("edr.osp.com") && url.equalsIgnoreCase("/test")) {
                FullHttpResponse resp = createContent("<script>alert('xss');</script>".getBytes());
                ctx.writeAndFlush(resp);
                return;
            }

//            if (host.contains("163.com")) {
//                DefaultFullHttpResponse resp = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, new HttpResponseStatus(302, "Moved Temporarily"));
//
//
//                resp.headers()
//                        .set(HttpHeaderNames.CONTENT_TYPE, "text/html")
//                        .set(HttpHeaderNames.SERVER, "nginx/1.1")
//                        .set(HttpHeaderNames.LOCATION, "http://www.hiapk.com/")
//                        .set(HttpHeaderNames.CACHE_CONTROL, "private")
//                        .set(HttpHeaderNames.CONNECTION, "Close");
//
//                ctx.writeAndFlush(resp);
//
//                System.out.println("redirect");
//                return;
//            }

            if (request.method() == HttpMethod.CONNECT) {//HTTPS建立代理握手

                connectToRemote(
                        ctx,
                        request,
                        new SSLProxyInitializer(ctx.channel()),
                        (ChannelFuture future) -> {
                            if (future.isSuccess()) {

                                HttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, new HttpResponseStatus(200, "Connection established"));
                                ctx.writeAndFlush(response);
                                ctx.pipeline().remove("httpCodec");
                                ctx.pipeline().remove("httpObject");

                            } else {
                                ctx.channel().close();
                            }

                        });

            } else {
                //连接至目标服务器
                connectToRemote(
                        ctx,
                        request,
                        new HttpProxyInitializer(ctx.channel(), request),
                        (ChannelFuture future) -> {
                            if (future.isSuccess()) {
                                future.channel().writeAndFlush(request);
                            } else {
                                ctx.channel().close();
                            }
                        });
            }

        } else { // https 只转发数据，不做处理
            if (cf != null) cf.channel().writeAndFlush(msg);
        }
    }

}