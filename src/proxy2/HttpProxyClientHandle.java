package proxy2;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.*;

public class HttpProxyClientHandle extends ChannelInboundHandlerAdapter {

    private Channel clientChannel;
    private FullHttpRequest request;

    public HttpProxyClientHandle(Channel clientChannel, FullHttpRequest request) {
        this.clientChannel = clientChannel;
        this.request = request;
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        clientChannel.close();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        FullHttpResponse response = (FullHttpResponse) msg;
        clientChannel.writeAndFlush(response);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        clientChannel.close();
    }
}