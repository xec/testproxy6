package proxy2;


import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;

public class HttpProxyInitializer extends ChannelInitializer {

    private Channel clientChannel;
    private FullHttpRequest request;

    public HttpProxyInitializer(Channel clientChannel, FullHttpRequest req) {
        this.clientChannel = clientChannel;
        this.request = req;
    }

    @Override
    protected void initChannel(Channel ch) throws Exception {
        ch.pipeline().addLast(new HttpClientCodec());
        ch.pipeline().addLast(new HttpObjectAggregator(100 * 1024 * 1024));
        ch.pipeline().addLast(new HttpProxyClientHandle(clientChannel, request));
    }
}