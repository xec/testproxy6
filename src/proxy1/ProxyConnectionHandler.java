package proxy1;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.buffer.Unpooled;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Handles a server-side channel.
 */
public class ProxyConnectionHandler extends ChannelInboundHandlerAdapter implements ProxyBackendListener {
    private volatile Channel outboundChannel;
    private volatile Channel inboundChannel;

    @Override
    public void onRemoteInactive(ChannelHandlerContext ctx) {
        inboundChannel.close();
    }

    @Override
    public void onRemoteRead(Object msg) {
        try {
            inboundChannel.writeAndFlush(msg).sync();
        } catch (InterruptedException e) {

        }
    }

    @Override
    public void onRemoteError(Throwable cause) {
        inboundChannel.close();
    }

    private enum StatusCode {
        Ready,
        HandleBody,
        SSLTunnel
    }

    private int content_length = 0;
    private int byte_sent = 0;
    private ByteBuf rbuf = Unpooled.EMPTY_BUFFER;
    private StatusCode statusCode = StatusCode.Ready;

    private String privHost;

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (outboundChannel != null) outboundChannel.close();
    }


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.read();
        inboundChannel = ctx.channel();
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        if (outboundChannel != null) outboundChannel.close();
    }

    private String readLine(ByteBuf buf) {
        StringBuilder line = new StringBuilder();
        byte ch;
        do {
            if (!buf.isReadable()) return null;
            ch = buf.readByte();
            if (ch == -1) return null;
            line.append((char) ch);
        } while (ch != '\n');
        return line.toString();
    }

    private String GetValue(ArrayList<String> headers, String key) {
        for (String entry : headers) {
            if (entry.startsWith(key)) {
                return entry.substring(entry.indexOf(':') + 1).trim();
            }
        }

        return "";
    }

    private void write(Channel channel, String line) {
        channel.writeAndFlush(Unpooled.wrappedBuffer(line.getBytes()));
    }

    private void write(Channel channel, ByteBuf buf) {
        channel.writeAndFlush(buf);

    }

    public void onConnectedRemote(String method, String uri, String version, ArrayList<String> headers, ByteBuf body, ChannelHandlerContext ctx) {

        if (!method.equals("CONNECT")) {

            String contentLengthString = GetValue(headers, "Content-Length");
            if (!contentLengthString.isEmpty()) {
                content_length = Integer.parseInt(contentLengthString);
                byte_sent = body.readableBytes();

                if (byte_sent < content_length) {
                    statusCode = StatusCode.HandleBody;
                }
            }

            write(outboundChannel, method + " " + uri + " " + version + "\r\n");
            for (String entry : headers.subList(1, headers.size()))
                write(outboundChannel, entry + "\r\n");

            write(outboundChannel, body);
        } else {
            //

            write(ctx.channel(), "HTTP/1.1 200 Connection established\r\n");
            write(ctx.channel(), "\r\n");
            statusCode = StatusCode.SSLTunnel;
        }
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, Object msg) throws Exception {
        try {

            if (rbuf.refCnt() == 0) {
                rbuf = Unpooled.copiedBuffer((ByteBuf) msg);
            } else {
                rbuf = Unpooled.copiedBuffer(rbuf, (ByteBuf) msg);
            }
            ((ByteBuf) msg).release();
            if (statusCode == StatusCode.Ready) {
//                System.out.println("new read event");


                String line;
                final ArrayList<String> headers = new ArrayList<>();

                // parse
                do {
                    line = readLine(rbuf);
                    if (line == null) {  // header large than 1024, so continue read next time.
                        rbuf.readerIndex(0);
                        headers.clear();
                        ctx.read();
                        return;
                    }
                    if (!line.startsWith("Proxy"))
                        headers.add(line.trim());
                } while (!line.equals("\r\n"));

                line = headers.get(0);
                StringTokenizer tokenizer = new StringTokenizer(line);
                final String method = tokenizer.nextToken();
                String url = tokenizer.nextToken();
                final String version = tokenizer.nextToken();

                System.out.println(method + " " + url);

                String hostAndPort = "";
                String uri = "/";

                if (method.equals("CONNECT")) {
                    hostAndPort = url;
                } else {
                    int idx = url.indexOf('/');
                    url = url.substring(idx + 2);
                    idx = url.indexOf('/');
                    uri = url.substring(idx);
                    hostAndPort = GetValue(headers, "Host");
                }

                String remoteHost, remotePort;
                if (hostAndPort.contains(":")) {
                    remoteHost = hostAndPort.substring(0, hostAndPort.indexOf(':'));
                    remotePort = hostAndPort.substring(hostAndPort.indexOf(':') + 1);
                } else {
                    remoteHost = hostAndPort;
                    remotePort = "80";
                }

                final String host = remoteHost;
                final String port = remotePort;
                final String uriString = uri;

                if (outboundChannel != null && privHost != null && !privHost.equals(host + ":" + port)) {
                    outboundChannel.close();
                    outboundChannel = null;
                }

                privHost = host + ":" + port;

                if (outboundChannel == null) {
                    Bootstrap b = new Bootstrap();
                    b.group(ctx.channel().eventLoop())
                            .channel(ctx.channel().getClass())
                            .handler(new ProxyBackendHandler(this))
                            .option(ChannelOption.AUTO_READ, false)
                            .option(ChannelOption.TCP_NODELAY, true)
                    ;
                    ChannelFuture f = b.connect(host, Integer.parseInt(port));
                    outboundChannel = f.channel();

                    f.addListener(new ChannelFutureListener() {
                        @Override
                        public void operationComplete(ChannelFuture future) {
                            if (future.isSuccess()) {
                                onConnectedRemote(method, uriString, version, headers, rbuf, ctx);
                                try {
                                    ctx.read();
                                } catch (Exception e) {

                                    System.out.println(e.getMessage());

                                }
                            } else {
                                System.out.println(future.cause().getMessage() + ": " + host + "," + port + "," + uriString);
                                ctx.close();
                            }
                        }
                    });
                } else {
                    onConnectedRemote(method, uriString, version, headers, rbuf, ctx);
                    ctx.read();
                }
            } else if (statusCode == StatusCode.HandleBody) {

                write(outboundChannel, rbuf);

                byte_sent += rbuf.readableBytes();
                if (byte_sent < content_length) {
                    statusCode = StatusCode.HandleBody;
                } else {
                    statusCode = StatusCode.Ready;
                }
                ctx.read();
            } else if (statusCode == StatusCode.SSLTunnel) {
                write(outboundChannel, rbuf);
                ctx.read();
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
            ctx.close();
        }

    } // (1)

    /**
     * Closes the specified channel after all queued write requests are flushed.
     */
    static void closeOnFlush(Channel ch) {
        if (ch.isActive()) {
            ch.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
        }
    }

}