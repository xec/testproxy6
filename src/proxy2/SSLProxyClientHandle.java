package proxy2;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class SSLProxyClientHandle extends ChannelInboundHandlerAdapter {
    private Channel clientChannel;

    public SSLProxyClientHandle(Channel clientChannel) {
        this.clientChannel = clientChannel;
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        clientChannel.close();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx0, Object msg) throws Exception {
        this.clientChannel.writeAndFlush(msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        clientChannel.close();
    }
}
